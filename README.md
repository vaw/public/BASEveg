## BASEveg: a python package for simulating riparian vegetation dynamics coupled with river morphodynamics ##

BASEveg simulates river morphodynamics coupled with vegetation dynamics, combining a 2D numerical model ([BASEMENT](https://basement.ethz.ch/)) and a python-based model for vegetation growth. The model builds up from the work by [Caponi et al. 2020](https://doi.org/10.1038/s41598-020-74106-9). A description of the main model features and of a test case is published in [Caponi et al. 2023](https://doi.org/10.1016/j.softx.2023.101361) 

## In this repository

The repositoriy is structured as follows:

- *BASEveg*: the source code;
- *tests*: simulations files for tests and tutorials; 
- *doc*: location for figures, related papers, and documentation materials in general.

The documentation is written as wiki page, which inlcudes a tutorial/user manual and a reference manual.

## Model applications

The model is inteded for studying eco-morphodynamics fluvial systems, where the interaction between plants, flow, and sediment transport is important. Possible applications of the model may inlcude:

- bed forms dynamics (e.g. alternate bars) and general river morphodynamic processes 
- sediment budgets and dynamics at the river-reach scales
- riparian vegetation succession and vegetation spatial distribution
- plant traits effect on river morphology
- restoration projects and re-naturalization of river corridors

## Acknowledgements

This work has been supported by the Swiss National Science Foundation under grant 159813. The authors acknowledge the financial support of the SNSF. 


